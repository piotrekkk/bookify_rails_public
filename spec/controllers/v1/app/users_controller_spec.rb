require 'rails_helper'

RSpec.describe V1::App::UsersController, type: :controller do
  describe 'GET #index' do
    it 'should list users' do
      user = sign_in_admin

      company = create :company
      create_list :user, 2, company: user.company

      get :index
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(parsed_response['collection'].count).to eq 3
    end

    it 'should list users only from user company' do
      user = sign_in_admin

      second_company = create :company
      create_list :user, 2, company: user.company
      create_list :user, 2, company: second_company

      get :index
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(parsed_response['collection'].count).to eq 3
    end

    it 'should list users only for admin' do
      sign_in_user

      create_list :user, 2

      get :index
      parsed_response = JSON.parse(response.body)
    end
  end

  describe 'POST #create' do
    it 'should create user with correct data' do
      user = sign_in_admin

      new_user = build :user, company: create(:company)
      password = { password: 'abcdefgh', password_confirmation: 'abcdefgh' }

      post :create, params: new_user.attributes.merge(password)
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :created
      expect(parsed_response['first_name']).to eql new_user.first_name
    end
  end

  describe 'GET #show' do
    it 'should show user' do
      user = sign_in_admin

      existing_user = create :user, company: create(:company)

      get :show, params: {
        id: existing_user
      }
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(parsed_response['first_name']).to eql existing_user.first_name
    end
  end

  describe 'PUT #update' do
    it 'should update user' do
      user = sign_in_admin

      put :update, params: {
        id: user.id,
        first_name: 'Piotr'
      }
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(parsed_response['first_name']).to eql 'Piotr'
    end
  end

  describe 'DELETE #destroy' do
    it 'should delete user' do
      user = sign_in_admin

      user_to_delete = create :user

      delete :destroy, params: {
        id: user_to_delete.id
      }
      expect(response).to have_http_status :no_content
    end
  end
end
