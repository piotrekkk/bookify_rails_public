require 'rails_helper'

RSpec.describe V1::App::CompaniesController, type: :controller do
  describe 'GET #show' do
    it 'should show company' do
      user = sign_in_user
      company = user.company

      get :show, params: {
        id: company.id
      }
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(parsed_response['name']).to eql company.name
    end
  end

  describe 'POST #create' do
    it 'should create company with correct data' do
      business_type = create :business_type

      post :create, params: {
        business_type_id: business_type.id,
        name: 'Company name',
        first_name: 'Jan',
        last_name: 'Kowalski',
        email: 'jan@kowalski.com',
        password: 'abcdefgh',
        role: 'admin'
      }
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :created
      expect(parsed_response['company']['name']).to eql 'Company name'
      expect(parsed_response['user']['first_name']).to eql 'Jan'
      expect(parsed_response['user']['auth_token']).not_to eql nil
    end
  end

  describe 'PUT #update' do
    it 'should update company' do
      user = sign_in_admin
      company = user.company

      put :update, params: {
        id: company.id,
        name: 'New name'
      }
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(parsed_response['name']).to eql 'New name'
    end
  end
end
