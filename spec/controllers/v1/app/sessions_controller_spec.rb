require 'rails_helper'

RSpec.describe V1::App::SessionsController, type: :controller do
  describe 'POST #create' do
    context 'user provides correct email and password' do
      it 'should sign in the user' do
        user = create :user, :without_auth_token

        post :create, params: {
          email: user.email,
          password: user.password
        }
        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(parsed_response['first_name']).to eql user.first_name
      end
    end

    context 'user provides incorrect email or password' do
      it 'should return bad request' do
        user = create :user, :without_auth_token

        post :create, params: {
          email: user.email,
          password: nil
        }
        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :bad_request
      end

      it 'should return bad request' do
        user = create :user, :without_auth_token

        post :create, params: {
          email: 'no',
          password: user.password
        }
        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :bad_request
      end
    end
  end

end
