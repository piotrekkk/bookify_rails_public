require 'rails_helper'

RSpec.describe V1::App::ServicesController, type: :controller do
  describe 'GET #index' do
    it 'should list services' do
      company = create :company
      create_list :service, 2, company: company

      get :index, params: { company_id: company.id }
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(parsed_response['collection'].count).to eq 2
    end

    it 'should list services only from user company' do
      user = sign_in_admin

      second_company = create :company
      create_list :service, 2, company: user.company
      create_list :service, 2, company: second_company

      get :index, params: { company_id: user.company.id }
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(parsed_response['collection'].count).to eq 2
    end

    it 'should raise an error when company_id param is not passed' do
      company = create :company
      create_list :service, 2, company: company

      expect { get :index, params: {} }.to raise_error(V1::App::ServicesController::NoCompanyIdParamError)
    end
  end

  describe 'GET #show' do
    it 'should show service' do
      service = create :service, company: create(:company)

      get :show, params: {
        id: service.id
      }
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(parsed_response['name']).to eql service.name
    end
  end

  describe 'POST #create' do
    it 'should create service' do
      user = sign_in_admin

      post :create, params: {
        name: 'New service',
        description: 'Description',
        price: 99.99
      }
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :created
      expect(parsed_response['name']).to eql 'New service'
      expect(parsed_response['price']).to eql 99.99
    end
  end

  describe 'PUT #update' do
    it 'should update service' do
      user = sign_in_admin
      service = create :service, company: user.company

      put :update, params: {
        id: service.id,
        name: 'New name'
      }
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(parsed_response['name']).to eql 'New name'
    end
  end

  describe 'DELETE #destroy' do
    it 'should delete user' do
      user = sign_in_admin

      service_to_delete = create :service

      delete :destroy, params: {
        id: service_to_delete.id
      }
      expect(response).to have_http_status :no_content
    end
  end
end
