require 'rails_helper'

RSpec.describe V1::App::BootController, type: :controller do
  describe 'GET #show' do
    context 'when user is authenticated' do
      it 'should return user and company' do
        user = sign_in_admin

        get :show, params: {
          user_auth_token: user.auth_token,
          user_auth_email: user.email
        }

        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(parsed_response['user']['first_name']).to eql user.first_name
        expect(parsed_response['company']['name']).to eql user.company.name
      end
    end

    context 'when user is not authenticated' do
      it 'should return unauthorized' do
        user = create :user, :without_auth_token

        get :show, params: {
          user_auth_token: 'that_will_not_work',
          user_auth_email: user.email
        }
        expect(response).to have_http_status :unauthorized
      end
    end
  end
end
