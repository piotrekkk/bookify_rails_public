require 'rails_helper'

RSpec.describe V1::Public::BusinessTypesController, type: :controller do
  describe 'GET #index' do
    it 'should show company' do
      create_list :business_type, 2

      get :index, params: {}
      parsed_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(parsed_response['collection'].count).to eq 2
    end
  end
end
