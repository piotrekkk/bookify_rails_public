FactoryGirl.define do
  factory :company, class: Company do
    business_type_id { create(:business_type).id }
    name { Faker::Company.name }
  end
end
