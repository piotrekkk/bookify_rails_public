FactoryGirl.define do
  factory :service, class: Service do
    name { Faker::Food.unique.dish }
    price { 19.99 }
    company_id { create(:company).id }
  end
end
