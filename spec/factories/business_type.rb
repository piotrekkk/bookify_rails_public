FactoryGirl.define do
  factory :business_type, class: BusinessType do
    name { Faker::Team.unique.state }
    description { Faker::Team.unique.name }
  end
end
