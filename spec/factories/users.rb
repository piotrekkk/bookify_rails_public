FactoryGirl.define do
  factory :admin, class: User do
    first_name { Faker::Name.name_with_middle }
    last_name { Faker::Name.name_with_middle }
    email { Faker::Internet.unique.email }
    role { :admin }
    password { Faker::Internet.password }
    password_confirmation { password }
    company_id { create(:company).id }
  end

  factory :user, class: User do
    first_name { Faker::Name.name_with_middle }
    last_name { Faker::Name.name_with_middle }
    email { Faker::Internet.unique.email }
    role { :user }
    password { Faker::Internet.password }
    password_confirmation { password }
    company_id { create(:company).id }

    trait :without_auth_token do
      after(:create) do |user|
        user.auth_token = nil
        user.token_expire_time = nil
      end
    end
  end
end
