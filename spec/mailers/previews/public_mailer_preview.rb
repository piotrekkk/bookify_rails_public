# Preview all emails at http://localhost:3000/rails/mailers/public_mailer
class PublicMailerPreview < ActionMailer::Preview
  def notification_to_office_email
    public_message = PublicMessage.first
    PublicMailer.notification_to_office_email(public_message)
  end

  def notification_to_client_email
    public_message = PublicMessage.first
    PublicMailer.notification_to_office_email(public_message)
  end
end
