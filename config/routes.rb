Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope 'api' do
    namespace 'v1' do
      scope module: 'app' do
        post 'sessions' => 'sessions#create'
        delete 'sessions' => 'sessions#destroy'
        get 'boot' => 'boot#show'

        resources 'business_types'
        resources 'companies'
        resources 'users'
        resources 'customers'
        resources 'services'
        resources 'visits'
        resources 'events', only: [:index]
      end

      namespace 'public' do
        resources 'business_types'
      end

      namespace 'mobile' do
        resources 'companies', only: [:index, :show]
        resources 'services', only: [:index, :show]
        resources 'business_types', only: [:index]
        resources 'visits', only: [:create]
      end
    end
  end

  resource :registration, only: [:new, :create, :thanks] do
    get 'thanks', :on => :member
  end

  resource :public_message, only: [:new, :create, :thanks] do
    get 'thanks', :on => :member
  end

  root to: 'landing_page#index'
end
