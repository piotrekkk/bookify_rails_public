class CreateVisits < ActiveRecord::Migration[5.2]
  def change
    create_table :visits do |t|
      t.datetime :date, presence: true
      t.references :company, foreign_key: true, index: false
      t.references :service, foreign_key: true, index: false
      t.references :user, foreign_key: true, index: false

      t.timestamps
    end
  end
end
