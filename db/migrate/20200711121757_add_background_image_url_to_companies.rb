class AddBackgroundImageUrlToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :background_image_url, :string
  end
end
