class AddBusinessTypeToCompany < ActiveRecord::Migration[5.2]
  def change
    add_reference :companies, :business_type, foreign_key: true, index: false
  end
end
