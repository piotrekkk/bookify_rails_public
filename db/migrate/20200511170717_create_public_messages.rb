class CreatePublicMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :public_messages do |t|
      t.text :message
      t.string :email
      t.string :phone
      t.string :name
      t.string :company_name

      t.timestamps
    end
  end
end
