class CreateBusinessTypes < ActiveRecord::Migration[5.2]
  def up
    create_table :business_types do |t|
      t.string :name, presence: true
      t.string :description, presence: true

      t.timestamps
    end
    add_index :business_types, :name, unique: true
  end

  def down
    drop_table :business_types
  end
end
