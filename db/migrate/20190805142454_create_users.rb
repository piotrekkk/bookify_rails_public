class CreateUsers < ActiveRecord::Migration[5.2]
  def up
    create_table :users do |t|
      t.string :first_name, presence: true
      t.string :last_name, presence: true
      t.date :birthdate
      t.string :email, presence: true
      t.string :password_digest

      t.timestamps
    end
    add_index :users, :email, unique: true
  end

  def down
    drop_table :users
  end
end
