class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :name, presence: true
      t.string :description, presence: true
      t.float :price, presence: true
      t.time :duration

      t.timestamps
    end

    add_index :services, :name, unique: true
  end
end
