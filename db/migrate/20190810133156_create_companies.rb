class CreateCompanies < ActiveRecord::Migration[5.2]
  def up
    create_table :companies do |t|
      t.string :name, presence: true

      t.timestamps
    end
    add_index :companies, :name, unique: true
  end

  def down
    drop_table :companies
  end
end
