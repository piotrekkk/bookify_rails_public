class ChangeServiceDurationToInteger < ActiveRecord::Migration[5.2]
  def change
    remove_column :services, :duration
    add_column :services, :duration, :integer, presence: true
  end
end
