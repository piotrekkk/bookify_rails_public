class AddCanceledAtToVisits < ActiveRecord::Migration[5.2]
  def change
    add_column :visits, :canceled_at, :datetime
  end
end
