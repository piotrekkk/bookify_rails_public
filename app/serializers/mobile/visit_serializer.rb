class Mobile::VisitSerializer < ActiveModel::Serializer
  attributes :id,
             :date

  has_one :company
  has_one :service
  has_one :user
end
