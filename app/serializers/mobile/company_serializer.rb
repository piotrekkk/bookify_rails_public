class Mobile::CompanySerializer < ActiveModel::Serializer
  attributes :id,
             :address,
             :background_image_url,
             :description,
             :name,
             :minimum_price

  has_one :business_type

  def minimum_price
    object.services.minimum(:price)
  end
end
