class ServiceSerializer < ActiveModel::Serializer
  attributes :id,
             :created_at,
             :updated_at,
             :description,
             :duration,
             :name,
             :price,
             :slots

  def slots
    object.available_slots
  end
end
