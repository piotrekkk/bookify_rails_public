class BusinessTypeSerializer < ActiveModel::Serializer
  attributes :id,
             :description,
             :name
end
