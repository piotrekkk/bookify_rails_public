class VisitSerializer < ActiveModel::Serializer
  attributes :id,
             :canceled_at,
             :created_at,
             :updated_at,
             :date,
             :service_name,
             :is_past

  has_one :company
  has_one :service
  has_one :user

  def service_name
    object.service.name
  end

  def is_past
    object.date < Time.now
  end
end
