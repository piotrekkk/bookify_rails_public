class CustomerSerializer < ActiveModel::Serializer
  attributes :id,
             :created_at,
             :updated_at,
             :birthdate,
             :first_name,
             :last_name,
             :email,
             :phone,
             :visits_count

  has_many :visits

  def visits_count
    object.visits.count
  end

  def visits
    object.visits.where(company_id: scope.company.id)
  end
end
