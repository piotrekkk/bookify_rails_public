class UserSerializer < ActiveModel::Serializer
  attributes :id,
             :auth_token,
             :created_at,
             :updated_at,
             :birthdate,
             :first_name,
             :last_name,
             :email,
             :role,
             :token_expire_time
end
