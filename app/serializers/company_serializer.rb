class CompanySerializer < ActiveModel::Serializer
  attributes :id,
             :address,
             :background_image_url,
             :description,
             :name

  has_one :business_type
end
