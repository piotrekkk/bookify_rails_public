class V1::Mobile::ServicesController < V1::Mobile::BaseController
  def index
    if permitted_params[:business_type_id] == "all"
      @services = Service.all
    elsif permitted_params[:business_type_id].present?
      @services = Service.joins(:company).where("companies.business_type_id = #{permitted_params[:business_type_id]}")
    elsif permitted_params[:company_id].present?
      @services = Service.where(company_id: permitted_params[:company_id])
    end
    render_collection(Service, @services, ServiceSerializer)
  end

  private

  def permitted_params
    params.permit(:business_type_id, :company_id)
  end
end
