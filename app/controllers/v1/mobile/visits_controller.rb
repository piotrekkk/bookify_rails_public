class V1::Mobile::VisitsController < V1::Mobile::BaseController
  def create
    @company = Company.find(regular_params[:company_id])
    @service = @company.services.find(regular_params[:service_id])

    @user = User.find_by(email: user_params[:email])
    if @user.present?
      @user.first_name = user_params[:first_name]
      @user.last_name = user_params[:last_name]
      @user.phone = user_params[:phone]
      @user.save
    else
      @user = User.create(first_name: user_params[:first_name], last_name: user_params[:last_name], email: user_params[:email], phone: user_params[:phone], role: :customer)
    end

    @visit = @company.visits.build(service: @service, user: @user, date: visit_date)

    if @visit.save
      render json: @visit, serializer: ::Mobile::VisitSerializer, status: :created
    else
      render json: @visit.errors, status: :unprocessable_entity
    end
  end

  private

  def regular_params
    params.permit(:company_id, :service_id)
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :phone, :email)
  end

  def date_params
    params.require(:date).permit(:day, :hour)
  end

  def visit_date
    year = 2020
    month = 9
    day = date_params[:day].split(",").last.strip.to_i
    hour = date_params[:hour].split(":").first.to_i
    minutes = date_params[:hour].split(":").last.to_i
    DateTime.new(year, month, day, hour, minutes)
  end
end
