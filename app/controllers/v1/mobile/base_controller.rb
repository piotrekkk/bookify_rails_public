class V1::Mobile::BaseController < ActionController::Base
  include CollectionsHelper
  protect_from_forgery
end
