class V1::Mobile::CompaniesController < V1::Mobile::BaseController
  def index
    if permitted_params[:business_type_id] == "all"
      @companies = Company.all
    else
      @companies = Company.where(business_type_id: permitted_params[:business_type_id])
    end
    render_collection(Service, @companies, ::Mobile::CompanySerializer)
  end

  private

  def permitted_params
    params.permit(:business_type_id)
  end
end
