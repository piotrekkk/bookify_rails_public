class V1::Mobile::BusinessTypesController < V1::Mobile::BaseController
  def index
    @business_types = BusinessType.all
    render_collection(BusinessType, @business_types, BusinessTypeSerializer)
  end
end
