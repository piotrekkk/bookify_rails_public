class V1::App::SessionsController < ApplicationController
  before_action :authenticate_user, only: :destroy

  def create
    email = create_params[:email]
    password = create_params[:password]

    if email.nil? or password.nil?
      render json: {}, status: :bad_request
      return
    end

    user = User.find_by_email(email)

    if user.present? && user.authenticate(password) && user.sign_in
      render json: user, serializer: UserSerializer, status: :ok
    else
      render json: {}, status: :bad_request
    end
  end

  def destroy
    if @current_user.nil?
      render json: {}, status: :unauthorized
    else
      @current_user.sign_out
      render json: {}, status: :ok
    end
  end

  private

  def create_params
    params.permit(:email, :password)
  end
end
