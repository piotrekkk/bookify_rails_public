class V1::App::UsersController < V1::App::BaseController
  before_action :authenticate_user

  def index
    @users = policy_scope(User)
    render_collection(User, @users, UserSerializer)
  end

  def show
    @user = User.find(params[:id])
    authorize @user

    render json: @user, serializer: UserSerializer
  end

  def create
    @user = User.new(permitted_attributes(User))
    authorize User

    if @user.save
      render json: @user, serializer: UserSerializer, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def update
    @user = User.find(params[:id])
    authorize @user

    if @user.update(permitted_attributes(@user))
      render json: @user, serializer: UserSerializer, status: :ok
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @user = User.find(params[:id])
    authorize User

    @user.destroy
    head :no_content
  end
end
