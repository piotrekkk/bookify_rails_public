class V1::App::BootController < V1::App::BaseController
  def show
    user = User.find_by_auth_token_and_email(permitted_params['user_auth_token'], permitted_params['user_auth_email'])

    if user.present? && user.token_expire_time.present? && user.token_expire_time > Time.now
      user.extend_token_time
      render json: {
        company: CompanySerializer.new(user.company),
        user: UserSerializer.new(user)
      }, status: :ok
    else
      render json: {}, status: :unauthorized
    end
  end

  private

  def permitted_params
    params.permit(:user_auth_token, :user_auth_email)
  end
end
