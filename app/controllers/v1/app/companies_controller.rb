class V1::App::CompaniesController < V1::App::BaseController
  before_action :authenticate_user, except: [:create]

  def index
    authorize @company
    render json: @company, serializer: CompanySerializer
  end

  def show
    @company = Company.find(params[:id])
    authorize @company

    render json: @company, serializer: CompanySerializer
  end

  def update
    @company = Company.find(params[:id])
    authorize @company

    if @company.update(permitted_attributes(@company))
      render json: @company, serializer: CompanySerializer, status: :ok
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  def create
    @company = Company.new(permitted_attributes(Company))
    @user = @company.users.new(permitted_attributes(User))
    authorize @company

    if @company.save
      # render json: @company, serializer: CompanySerializer, status: :created
      render json: {
        company: CompanySerializer.new(@company),
        user: UserSerializer.new(@user)
      }, status: :created
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end
end
