class V1::App::EventsController < V1::App::BaseController
  before_action :authenticate_user

  def index
    events = company.visits.map do |visit|
      {
        "id": visit.id,
        "title": visit.service.name,
        "start": visit.date.change(:offset => "+01:00").to_f * 1000,
        "end": (visit.date.change(:offset => "+01:00") + visit.service.duration.minutes).to_f * 1000,
        "allDay": false
      }
    end

    render json: events.to_json
  end
end
