class V1::App::BaseController < ActionController::Base
  include AuthHelper
  include CollectionsHelper
  include Pundit
  protect_from_forgery

  def pundit_params_for(record)
    params
  end
end
