class V1::App::ServicesController < V1::App::BaseController
  before_action :authenticate_user

  def index
    @services = policy_scope(Service).where(company_id: company.id)
    render_collection(Service, @services, ServiceSerializer)
  end

  def show
    @service = Service.find(params[:id])
    authorize @service

    render json: @service, serializer: ServiceSerializer
  end

  def create
    @service = Service.new(permitted_attributes(Service))
    @service.company = @company
    authorize Service

    if @service.save
      render json: @service, serializer: ServiceSerializer, status: :created
    else
      render json: @service.errors, status: :unprocessable_entity
    end
  end

  def update
    @service = Service.find(params[:id])
    authorize @service

    if @service.update(permitted_attributes(@service))
      render json: @service, serializer: ServiceSerializer, status: :ok
    else
      render json: @service.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @service = Service.find(params[:id])
    authorize Service

    @service.destroy
    head :no_content
  end

  private

  def permitted_params
    params.permit(:company_id)
  end
end
