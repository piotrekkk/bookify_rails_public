class V1::App::BusinessTypesController < ApplicationController
  def index
    @business_types = policy_scope(BusinessType)
    render_collection(BusinessType, @business_types, BusinessTypeSerializer)
  end
end
