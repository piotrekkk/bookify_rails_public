class V1::App::VisitsController < V1::App::BaseController
  before_action :authenticate_user

  def index
    @visits = policy_scope(Visit)
    render_collection(Visit, @visits, VisitSerializer)
  end

  def show
    @visit = company.visits.find(params[:id])
    authorize @visit

    render json: @visit, serializer: VisitSerializer
  end

  def create
    @service = company.services.find(regular_params[:service_id])
    @customer = User.find_by(id: regular_params[:customer_id]) || User.create(first_name: user_params[:first_name], last_name: user_params[:last_name], email: user_params[:email], phone: user_params[:phone], role: :customer)
    @visit = @company.visits.build(service: @service, user: @customer, date: visit_date)

    if @visit.save
      render json: @visit, serializer: VisitSerializer, status: :created
    else
      render json: @visit.errors, status: :unprocessable_entity
    end
  end

  def update
    @visit = company.visits.find(params[:id])
    authorize @visit

    if @visit.update(permitted_attributes(@visit))
      AppMailer.visit_canceled_email(@visit).deliver_now if @visit.saved_change_to_canceled_at?
      render json: @visit, serializer: VisitSerializer, status: :ok
    else
      render json: @visit.errors, status: :unprocessable_entity
    end
  end

  private

  def regular_params
    params.permit(:customer_id, :service_id, :day, :slot, :canceled_at)
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :phone, :email)
  end

  def date_params
    params.require(:date).permit(:day, :hour)
  end

  def visit_date
    year = 2020
    month = 9
    day = date_params[:day].split(",").last.strip.to_i
    hour = date_params[:hour].split(":").first.to_i
    minutes = date_params[:hour].split(":").last.to_i
    DateTime.new(year, month, day, hour, minutes)
  end
end
