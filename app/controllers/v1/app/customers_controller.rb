class V1::App::CustomersController < V1::App::BaseController
  before_action :authenticate_user

  def index
    @customers = User.with_visits.for_company(company.id)
    render_collection(User, @customers, CustomerSerializer)
  end

  def show
    @customer = User.find(params[:id])
    authorize @customer

    render json: @customer, serializer: CustomerSerializer
  end
end
