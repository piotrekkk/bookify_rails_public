module CollectionsHelper extend ActiveSupport::Concern

  include ActionController::Serialization
  include SortingHelper
  include PaginationHelper

  def render_collection(model, collection, each_serializer, parameters = {})
    setup_pagination(page: params[:page], page_size: params[:page_size])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order], model: model)
    # collection = search_collection(collection, search_field: params[:search_field], search_value: params[:search_value])
    total_size = collection.size
    collection = collection.page(@page).per(@page_size)
    collection = collection.order(@sort_by => @sort_order)

    collection_json = ActiveModelSerializers::SerializableResource.new(collection, serializer: CollectionSerializer, each_serializer: each_serializer, parameters: parameters, scope: current_user)

    json = {
      TOTAL_SIZE_LABEL => total_size,
      PAGE_LABEL => @page,
      PAGE_SIZE_LBAEL => @page_size,
      COLLECTION_LABEL => collection_json
    }

    render json: json, adapter: :json
  end
end
