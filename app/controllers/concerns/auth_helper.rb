module AuthHelper extend ActiveSupport::Concern

  include ActionController::HttpAuthentication::Token::ControllerMethods

  def authenticate_user
    authenticate_or_request_with_http_token do |token, options|
      email = request.headers[HEADER_AUTH_EMAIL]
      tmp_user = User.find_by_auth_token_and_email(token, email)

      if tmp_user.present? && tmp_user.token_expire_time.present? && tmp_user.token_expire_time > Time.now
        tmp_user.extend_token_time
        @current_user = tmp_user
        @company = tmp_user.company
      else
        render status: :unauthorized,
               json: {}
        return
      end
    end
  end

  def current_user
    @current_user
  end

  def company
    @current_user.company
  end
end
