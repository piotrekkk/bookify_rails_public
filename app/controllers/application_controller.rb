class ApplicationController < ActionController::Base
  include AuthHelper
  include CollectionsHelper
  include Pundit
  protect_from_forgery

  # before_action :get_company

  def pundit_params_for(record)
    params
  end
end
