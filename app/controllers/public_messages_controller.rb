class PublicMessagesController < ActionController::Base
  layout 'landing_page'

  def new
  end

  def thanks
  end

  def create
    public_message = PublicMessage.create(public_message_params)
    PublicMailer.notification_to_office_email(public_message).deliver_now
    PublicMailer.notification_to_client_email(public_message).deliver_now

    redirect_to thanks_public_message_path
  end

  private

  def public_message_params
    params.require(:public_message).permit(:company_name, :email, :message, :name, :phone)
  end
end
