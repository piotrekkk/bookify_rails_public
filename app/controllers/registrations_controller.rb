class RegistrationsController < ActionController::Base
  layout 'landing_page'

  def new
    @business_types = BusinessType.all
  end

  def thanks
  end

  def create
    @company = Company.new(company_params)
    @user = @company.users.new(user_params)

    if @company.save && @user.save
      redirect_to thanks_registration_path
    end
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password).merge(role: :admin);
  end

  def company_params
    params.require(:company).permit(:name, :business_type_id)
  end
end
