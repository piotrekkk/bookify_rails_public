class PublicMailer < ApplicationMailer
  default from: "bookify.com.pl"
  layout "mailer"

  def notification_to_office_email(public_message)
    @public_message = public_message
    mail(to: "klupa.piotr@gmail.com", subject: "Nowa wiadomość z bookify.com.pl")
  end

  def notification_to_client_email(public_message)
    @public_message = public_message
    mail(to: public_message.email, subject: "Dziękujemy za kontakt!")
  end
end
