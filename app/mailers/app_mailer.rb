class AppMailer < ApplicationMailer
  default from: "bookify.com.pl"
  layout "mailer"

  def visit_canceled_email(visit)
    @date = visit.date.strftime("%A, %H:%M %d %B %Y")
    @user = visit.user
    mail(to: @user.email, subject: "Twoja wizyta została odwołana")
  end
end
