const simpleFieldValidator = value => {
  return value.length !== 0;
};

const emailValidator = value => {
  const emailRegexp = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
  return emailRegexp.test(value);
};

const passwordValidator = value => {
  return value && value.length >= 6;
};

const selectValidator = value => {
  return !!value;
};

const handleValidationChange = (isValid, field) => {
  const errorLabel = field.parentNode.querySelector('.c-input__error');

  if (isValid) {
    errorLabel.classList.add('u-display--none');
  } else {
    errorLabel.classList.remove('u-display--none');
  }
};

const validateRegistrationForm = () => {
  const form = document.querySelector('.c-registration-page__content__form');
  if (!form) return;
  const submitButton = form.querySelector('input[type="submit"]');
  const fields = {
    'company[name]': form.querySelector('input[name="company[name]"]'),
    'user[first_name]': form.querySelector('input[name="user[first_name]"]'),
    'user[last_name]': form.querySelector('input[name="user[last_name]"]'),
    'user[email]': form.querySelector('input[name="user[email]"]'),
    'user[password]': form.querySelector('input[name="user[password]"]')
  };
  let isCompanyNameValid, isCompanyBusinessTypeValid, isUserFirstNameValid, isUserLastNameValid, isUserEmailValid, isUserPasswordValid = false;

  form.addEventListener('change', event => {
    const fieldName = event.target.name;
    const fieldValue = event.target.value;

    switch (fieldName) {
      case 'company[name]': {
        isCompanyNameValid = simpleFieldValidator(fieldValue);
        handleValidationChange(isCompanyNameValid, fields[fieldName]);
        break;
      }
      case 'company[business_type_id]': {
        isCompanyBusinessTypeValid = selectValidator(fieldValue);
        break;
      }
      case 'user[first_name]': {
        isUserFirstNameValid = simpleFieldValidator(fieldValue);
        handleValidationChange(isUserFirstNameValid, fields[fieldName]);
        break;
      }
      case 'user[last_name]': {
        isUserLastNameValid = simpleFieldValidator(fieldValue);
        handleValidationChange(isUserLastNameValid, fields[fieldName]);
        break;
      }
      case 'user[email]': {
        isUserEmailValid = emailValidator(fieldValue);
        handleValidationChange(isUserEmailValid, fields[fieldName]);
        break;
      }
      case 'user[password]': {
        isUserPasswordValid = passwordValidator(fieldValue);
        handleValidationChange(isUserPasswordValid, fields[fieldName]);
        break;
      }
    }

    const isFormValid = isCompanyNameValid && isCompanyBusinessTypeValid && isUserFirstNameValid && isUserLastNameValid && isUserEmailValid && isUserPasswordValid;
    if (isFormValid) {
      submitButton.removeAttribute('disabled');
    } else {
      submitButton.setAttribute('disabled', true);
    }
  });
};

const validateContactForm = () => {
  const form = document.querySelector('.c-contact-page__content__form');
  if (!form) return;
  const submitButton = form.querySelector('input[type="submit"]');
  const fields = {
    'public_message[name]': form.querySelector('input[name="public_message[name]"]'),
    'public_message[email]': form.querySelector('input[name="public_message[email]"]'),
    'public_message[message]': form.querySelector('textarea[name="public_message[message]"]')
  };
  let isNameValid, isEmailValid, isMessageValid = false;

  form.addEventListener('change', event => {
    const fieldName = event.target.name;
    const fieldValue = event.target.value;

    switch (fieldName) {
      case 'public_message[name]': {
        isNameValid = simpleFieldValidator(fieldValue);
        handleValidationChange(isNameValid, fields[fieldName]);
        break;
      }
      case 'public_message[email]': {
        isEmailValid = emailValidator(fieldValue);
        handleValidationChange(isEmailValid, fields[fieldName]);
        break;
      }
      case 'public_message[message]': {
        isMessageValid = simpleFieldValidator(fieldValue);
        handleValidationChange(isMessageValid, fields[fieldName]);
        break;
      }
    }

    const isFormValid = isNameValid && isEmailValid && isMessageValid;
    if (isFormValid) {
      submitButton.removeAttribute('disabled');
    } else {
      submitButton.setAttribute('disabled', true);
    }
  });
};

document.addEventListener('turbolinks:load', function() {
  validateRegistrationForm();
  validateContactForm();
}, false);
