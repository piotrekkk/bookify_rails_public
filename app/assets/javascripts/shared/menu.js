const handleMenu = () => {
  let isOpened = false;
  const menuIconClassName = 'c-header__nav-mobile__menu-icon';
  const openIconClassModifier = '--open';
  const closeIconClassModifier = '--close';

  Array.from(document.getElementsByClassName(menuIconClassName)).forEach(element => {
    element.addEventListener('click', () => {
      const openIcon = document.querySelector(`.${menuIconClassName}${openIconClassModifier}`);
      const closeIcon = document.querySelector(`.${menuIconClassName}${closeIconClassModifier}`);
      const header = document.querySelector('.c-header');

      if (isOpened) {
        header.classList.remove('c-header--menu-opened');
        openIcon.classList.remove('u-display--none');
        closeIcon.classList.add('u-display--none');
      } else {
        header.classList.add('c-header--menu-opened');
        openIcon.classList.add('u-display--none');
        closeIcon.classList.remove('u-display--none');
      }

      isOpened = !isOpened;
    });
  });
};

document.addEventListener('DOMContentLoaded', () => {
  handleMenu();
}, false);

document.addEventListener('turbolinks:load', () => {
  handleMenu();
});
