class AvailableSlotsService
  def initialize(service_id, company_id, service_duration, todays_date)
    @service_id = service_id
    @company_id = company_id
    @service_duration = service_duration
    @todays_date = todays_date
  end

  def call
    result = []
    days_in_month(current_month).map do |day|
      day_name = Time.new(2020, current_month, day).strftime("%a")
      day_result = { day: "#{day_name}, #{day}", slots: [] }

      current_time = start_day_hour
      begin
        unless is_visit_at?(current_month, day, current_time.strftime("%H").to_i, current_time.strftime("%M").to_i)
          day_result[:slots].push(current_time.strftime("%H:%M"))
        end
        current_time += @service_duration.minutes
      end while current_time < end_day_hour

      result.push(day_result)
    end

    result
  end

  private

  def service
    @service ||= Service.find(@service_id)
  end

  def start_day_hour
    @todays_date.change(hour: 8)
  end

  def end_day_hour
    @todays_date.change(hour: 17)
  end

  def days_in_month(month)
    days_count = Time.days_in_month(month)

    if month == @todays_date.month
      days_array = (get_soonest_possible_start_day..days_count).to_a
    else
      days_array = (1..days_count).to_a
    end

    days_array
  end

  def get_soonest_possible_start_day
    if (@todays_date + @service_duration.minutes) <= end_day_hour
      @todays_date.day
    else
      @todays_date.tomorrow.day
    end
  end

  def is_visit_at?(month, day, hour, minute)
    slot_time = DateTime.new(2020, month, day, hour, minute).to_datetime
    service_visits.find { |visit| visit.date.to_datetime == slot_time && visit.canceled_at.blank? }.present?
  end

  def current_month
    9
  end

  def company
    @company ||= Company.find(@company_id)
  end

  def service_visits
    @service_visits ||= company.visits.where(service_id: @service_id)
  end
end
