class UserPolicy < ApplicationPolicy
  def permitted_attributes
    [
      :email,
      :first_name,
      :last_name,
      :password,
      :password_confirmation,
      :role,
      :company_id
    ]
  end

  def index?
    user.admin?
  end

  def create?
    user.admin?
  end

  def show?
    true
  end

  def update?
    user.admin? || user.id == record.id
  end

  def destroy?
    user.admin?
  end

  class Scope < Scope
    def resolve
      scope.all.where(company_id: user.company_id)
    end
  end
end
