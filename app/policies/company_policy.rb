class CompanyPolicy < ApplicationPolicy
  def permitted_attributes
    [
      :address,
      :background_image_url,
      :business_type_id,
      :description,
      :name,
    ]
  end

  def index?
    user.admin?
  end

  def create?
    true
  end

  def show?
    user.admin? || user.company.id == record.id
  end

  def update?
    user.admin?
  end

  class Scope < Scope
    def resolve
      scope.where(id: user.company.id)
    end
  end
end
