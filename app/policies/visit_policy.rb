class VisitPolicy < ApplicationPolicy
  def permitted_attributes
    [
      :canceled_at
    ]
  end

  def index?
    user.admin?
  end

  def show?
    true
  end

  def update?
    user.admin?
  end

  class Scope < Scope
    def resolve
      scope.all.where(company_id: user.company_id)
    end
  end
end
