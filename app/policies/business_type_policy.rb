class BusinessTypePolicy < ApplicationPolicy
  def permitted_attributes
    [
      :name,
      :description,
    ]
  end

  def create?
    true
  end

  def show?
    user.admin? || user.company.id == record.id
  end

  def update?
    user.admin?
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
