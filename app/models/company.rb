class Company < ApplicationRecord
  belongs_to :business_type

  has_many :users
  has_many :services
  has_many :visits

  validates :name, presence: true
  validates :business_type_id, presence: true
end
