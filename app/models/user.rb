class User < ApplicationRecord
  has_secure_password validations: false

  belongs_to :company, optional: true
  has_many :visits

  enum role: { admin: 0, user: 1, customer: 2 }

  validates :first_name, presence: true, length: { minimum: USER_NAME_MIN_LENGTH }
  validates :last_name, presence: true, length: { minimum: USER_NAME_MIN_LENGTH }
  validates :role, inclusion: { in: ['admin', 'user', 'customer'] }
  validates :email, presence: true, uniqueness: true
  validates_presence_of :password, if: :should_validate_password?, on: :create

  before_create :set_auth_token

  scope :for_company, -> (company_id) { joins(:visits).where(role: 2, visits: { company_id: company_id }).distinct }
  scope :with_visits, -> { includes(:visits) }

  def sign_in
    set_auth_token
    save
  end

  def sign_out
    self.auth_token = nil
    self.token_expire_time = nil
    save
  end

  def set_auth_token
    self.auth_token = SecureRandom.uuid.gsub(/\-/, '')
    extend_token_time
  end

  def extend_token_time
    self.token_expire_time = 1.week.from_now
  end

  private

  def should_validate_password?
    return true unless self.customer?
    false
  end
end
