class Visit < ApplicationRecord
  belongs_to :company, required: true
  belongs_to :service, required: true
  belongs_to :user, required: true

  validates :date, presence: true
end
