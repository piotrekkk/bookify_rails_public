class Service < ApplicationRecord
  belongs_to :company

  validates :company, presence: true
  validates :name, presence: true
  validates :price, presence: true

  def available_slots
    AvailableSlotsService.new(self.id, self.company.id, self.duration, Time.now).call
  end
end
